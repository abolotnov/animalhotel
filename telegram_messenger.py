from telegram import Bot
from conf import TELEGRAM_API_KEY, TELEGRAM_CHANNEL_NAME, TELEGRAM_MESSAGE_ON_BOT_START, TELEGRAM_MESSAGE_ON_BOT_START_TEXT


class TelegramMessageSender:

    def __init__(self):
        self.bot = Bot(token=TELEGRAM_API_KEY)
        if TELEGRAM_MESSAGE_ON_BOT_START:
            self.bot.send_message(TELEGRAM_CHANNEL_NAME, TELEGRAM_MESSAGE_ON_BOT_START_TEXT)

    def send_message(self, text, image=None):
        if not image:
            self.bot.send_message(TELEGRAM_CHANNEL_NAME, text)
        else:
            self.bot.send_photo(chat_id=TELEGRAM_CHANNEL_NAME, caption=text, photo=open(image, 'rb'))
