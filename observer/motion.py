"""
"Calibrated" Motion detection with PIR motion sensor:

- only fires events on specified number of motions detected within a time window
- 50+ queue_len seems to help smoothen against false positives
- timer and sensor run in separate threads but use a class-level counter to sync on motions count
- run the logic for your actions in the do_action() method

"""

import logging
from logging.handlers import RotatingFileHandler
from gpiozero import MotionSensor
from camera import Cam
import time
import os
import datetime
import threading
import configparser
import sys

#setup logging from default.ini
_conf = configparser.ConfigParser()
_conf.read(os.path.join(os.path.dirname(sys.argv[0]), "default.ini"))
_log_path = _conf["Logging"]["file_path"]
logging.basicConfig(level=logging.DEBUG, format="%(asctime)s:%(levelname)s:%(funcName)s %(message)s", filename=_log_path)


class MotionObserver:

    def __init__(self, config_file = 'default.ini'):
        self.status_file = _conf["General"]["status_file"]
        self.status_interval = float(_conf["General"]["status_interval"])
        self.start_date = datetime.datetime.now()
        self.actions_since_start = 0
        self.motion_pin = int(_conf["MotionSensor"]["sensor_pin"])
        self.motion_observer_interval = float(_conf["MotionSensor"]["observer_interval"])
        self.queue_len = int(_conf["MotionSensor"]["queue_len"])
        try:
            self.motion_sensor = MotionSensor(self.motion_pin, queue_len=self.queue_len)
        except Exception as e:
            logging.critical(f"Error initializing sensor, very likely already initialized by another process. {e}")
            sys.exit("Cannot initialize motion sensor, see logs")
        self.events_counter = 0
        self.timer_interval = float(_conf["MotionSensor"]["timer_interval"])
        self.event_threshold = int(_conf["MotionSensor"]["events_threshold"])
        try:
            self.cam = Cam()
        except Exception as e:
            logging.critical(f"Cannot initialize camera, very likely busy by another programm: {e}")
            sys.exit("Cannot initialize camera, see logs")
        threading.Thread(target=self.generate_status).start()
        logging.info("Motion Observer initialized")

    def generate_status(self):
        while (True):
            with open(self.status_file, 'w') as file:
                logging.info("Writing metrics")
                file.write(f"Started at {self.start_date}, actions since start: {self.actions_since_start}, time sampled: {datetime.datetime.now()}")
            time.sleep(self.status_interval)

    def observe_threaded(self):
        #this method will endlessly report on motion events while stop_flag is still False
        while(True):
            self.motion_sensor.wait_for_motion()
            logging.info("Motion detected")
            self.events_counter+=1
            time.sleep(self.motion_observer_interval)
                    
    def timer(self):
        #timer will reset a counter and trigger action on threshold until stopped by the stop timer
        while (True):
            time.sleep(self.timer_interval)
            if self.events_counter >= self.event_threshold:
                logging.info(f"{self.events_counter} events since {self.timer_interval}s, calling action")
                self.do_action()
            else:
                logging.info(f"{self.events_counter} events since {self.timer_interval}s, no action")
            self.events_counter = 0

    def run(self):
        #run the motion detector and the timer in separate threads
        _timer = threading.Thread(target=self.timer)
        _obs = threading.Thread(target=self.observe_threaded)
        _timer.start()
        _obs.start()

    def do_action(self):
        #method that does the actions
        self.actions_since_start+=1
        logging.info("do_action triggered")
        try:
            _picpath = self.cam.take_picture()
            logging.info(f"Got picture from cam: {_picpath}")
        except Exception as e:
            logging.exception(f"exception while perfoming the action: {e}")


if __name__ == "__main__":
    system = MotionObserver()
    #system.run()
