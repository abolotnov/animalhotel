from picamera import PiCamera
import logging
import time
import os

class Cam:

    def __init__(self):
        logging.info("initializing camera...")
        
        #camera and pictures settings
        self.picfile_path = "/var/www/html/pics"
        self.picture_name = "picture.jpg"
        self.max_pics = 100
        self.resolution = (1024, 768)
        
        #start camera
        self.camera = PiCamera()
        self.camera.resolution = self.resolution
        self.camera.start_preview()
        time.sleep(1)

        self.pic_counter = 0
                
    def take_picture(self) -> str:
        if self.pic_counter >= self.max_pics:
            self.pic_counter = 0
        _picture_full_path = os.path.join(self.picfile_path, "{}-{}".format(self.pic_counter, self.picture_name))
        try:
            self.camera.capture(_picture_full_path)
            logging.info(f"Camera took a picture {_picture_full_path}")
            self.pic_counter+=1
        except Exception as e:
            logging.exception(f"Camera failed to take a picture, raising exception")
            raise e
        return _picture_full_path
