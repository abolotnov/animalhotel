# Observer

This is a better version of the motion detection package with PIR sensor and RPI Camera:
- It can be installed as a service and controlled with `systemctl`
- It does not use `motion` software and camera-based motion recognition (too much CPU on RPI Zero and not very flexible to configure for accurate motion detection)
- Since the above, you will need a PIR motion detection sensor

## Installation

I use RPI Zero and run everything as `pi` user, repo cloned in the `/home/pi` directory. You may need to tweack the settings if you are using a different setup.

- install lighttpd with `sudo apt-get install lighttpd`
- copy the lighttpd config from this repo to `etc`: `cp ./lighttpd.conf /etc/lighttpd/`
- change the owner of the `/var/www/` directory to `pi`: `sudo shown -R pi: /var/www`
- copy the `motionbox.service` file to `~/.config/systemd/user` - this way we can control the app as the service with `systemctl`
