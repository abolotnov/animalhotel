from argparse import ArgumentParser

def echo():
    print(f"Your --text argument is {args.text}")

def telegram():
    from telegram_messenger import TelegramMessageSender
    from timer import Timer
    timer = Timer(system="telegram")
    if timer.can_run:
        TelegramMessageSender().send_message(text=args.text, image=args.image)
        timer.save()
        print("Done sending the message.")
    else:
        print(f"Sending message too early, you need to wait for {timer.seconds_until_next_run} seconds.")
    
parser = ArgumentParser('AnimalHotelCLI')
subparsers = parser.add_subparsers(dest='action')

#echo parser
echo_parser = subparsers.add_parser('echo', help='Harmless echo action')
echo_parser.add_argument('--text', help="Text to echo", required=True)

#sendtotelegram parser
telegram_messager_perser = subparsers.add_parser('telegram', help='Send message to telegram channel (the API key and Channel name come from config)')
telegram_messager_perser.add_argument('--text', help='Text to send', required=True)
telegram_messager_perser.add_argument('--image', help='Optional Image to attach', required=False)

args = parser.parse_args()

if args.action == 'echo':
    echo()
elif args.action == 'telegram':
    telegram()
else:
    parser.print_help()



