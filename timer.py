import logging
import time
import os
import importlib

class Timer:
#This could be static, but I am using all bound methods since we need to make sure file exists
#I use self.file from TIME_FILE setting to treak PyLint into thinking that the methods can be legit bound
    def __init__(self, system):
        #system should allow using different timers for different systems
        self.system = system
        try:
            conf = importlib.import_module("conf")
            self.file = getattr(conf, "{}_TIME_FILE".format(str.upper(self.system)))
            self.delay = getattr(conf, "{}_DELAY_SECONDS".format(str.upper(self.system)))
        except Exception as e:
            raise(e)
        if os.path.exists(self.file) is not True:
            logging.info(f"{self.file} does not exist, creating it with 0 value")
            with open(self.file, "w") as file:
                file.write("0")

    @property
    def seconds_since_last_run(self) -> int:
        time_file_data = int(float(open(self.file, 'r').read()))
        return int(time.time()-time_file_data)

    @property
    def seconds_until_next_run(self) -> int:
        if self.can_run is True:
            return 0
        else:
            return self.delay - self.seconds_since_last_run

    @property
    def can_run(self) -> bool:
        if self.seconds_since_last_run >= self.delay:
            return True
        else:
            return False
        raise Exception("Could not calculate time")

    def save(self):
        try:
            with open(self.file, 'w') as time_file:
                time_file.write(str(time.time()))
        except IOError as error:
            logging.error(f"Cannot access the time file: {error}")
        except Exception as exception:
            logging.error(f"Some exception occured: {exception}")
