# Animal Hotel

It's a box with a camera inside to let animals hang out in it in bad weather and watch them do funny things.

The idea is to do a few things:

- Monitor motion and send a message with a pic to a telegram channel
- Record a motion video and send to chat
- Have a streaming services to be able to watch them do things there :)
- Have some sort of LED lights in the box so that camera can make a good picture

It's a simple project I'm doing with my kids, nothing too sophisticated. You can use this to build a cheap monitoring and signalling thing.

## Setup, if you want to build one yourself

- There is no package/installation script right now so it's manual for now 
- Install `motion` and configure it (see on motion configuration below)
- Check `conf.py` for paths and stuff
- Check that `motion` user can execute the script and has access to the files the scripts use in `conf.py`

## How it works

Everything is orchestrated by the `cli.py` file which will run commands.
At this point, I only have the `echo` (just a test) and `telegram` commands implemented.

## Configure telegram

`telegram` command in `cli.py` will send a message to your configured telegram channel (optionally with an image).

Before you can do this, you need to update the configuration:
- add your bot key to `telegram.key` file
- update `conf.py` with TELEGRAM_ settings that are right for you

If everything is properly configured you should be able to do this to send a message to telegram:

```
python3 cli.py --text="Sample text" --image="~/images/some-image.jpg"
```

## Motion

I use motion to monitor motion detection and record videos or detected activities and take pictures. Here are a few of my settings in `/etc/motion/motion.conf`

```python
picture_type jpeg
picture_filename motion-recent
target_dir /var/lib/motion
on_motion_detected python3 /home/pi/AnimalHotel/cli.py telegram --text "automatically detected motion" --image "/var/lib/motion/motion-recent.jpg"
```

The motion detection can really flood your telegram so the script has a limitation to send to telegram only so few seconds, per `TELEGRAM_DELAY_SECONDS` setting. The setting `threshold 1500` in `motion.conf` drives how frequent the detection will be. You will need to play with this setting to find what works for you and your box.

## Timer class

`Timer` class in `timer.py` is a little utility to monitor frequency of events. For instance, `on_motion_detected` in `motion` can happen every second. I don't want to send messages to telegram every second, but motion does not have functionality around limiting the frequency of events in fires. So I had to come up with my own, hence the `Timer`.

### configuration

timer can be configured for different systems via `conf.py` settings:

```python
#{SYSTEM}_TIME_FILE - the path to file timer will use to keep time in for this systems
#{SYSTEM}_DELAY_SECONDS - the delay in seconds before timer considers it's ok to fire another event for this systems
TELEGRAM_TIME_FILE = '/var/log/tools/telegram_time'
TELEGRAM_DELAY_SECONDS = 90
```
### usage

```python
from timer import Timer
my_timer = Timer(system='telegram')
if my_timer.can_run:
    print("Enough time lapsed since last run, going to do something here")
    my_timer.save() #this will tell the timer that you did an action, otherwise it would not know you did
else:
    print(f"Timer cannot run yet, {my_timer.seconds_since_last_run} only since last run, you need to wait for {my_timer.seconds_until_next_run} seconds")
```
