import os
import sys

#telegram configuration
_keypath = os.path.join(os.path.dirname(sys.argv[0]), "telegram.key")
TELEGRAM_API_KEY = open(_keypath, "r").read()
TELEGRAM_CHANNEL_NAME = '@AnimalHotel'
TELEGRAM_MESSAGE_ON_BOT_START = False
TELEGRAM_MESSAGE_ON_BOT_START_TEXT = 'The bot has started!'

#telegram timer settings
TELEGRAM_TIME_FILE = '/var/log/tools/telegram_time'
TELEGRAM_DELAY_SECONDS = 90

#motion settings
SNAPSHOT_DEFAULT_FILE = '/var/lib/motion/motion-recent.jpg'

#logging
LOG_FILE = '/var/log/tools/tools.log'
