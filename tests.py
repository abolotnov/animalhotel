import pytest
from tools import Timer
import time


def test_timer_returns_int():
    assert type(Timer().seconds_since_last_run()) is int

def test_timer_sets_properly():
    print(f"Timer current value: {Timer().seconds_since_last_run()}")
    print(f"Timer is ok? {Timer().is_delay_ok()}")
    Timer().save_timer()
    time.sleep(2)
    assert 2 == Timer().seconds_since_last_run()
    